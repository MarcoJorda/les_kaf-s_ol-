﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bataille_Cafe
{
    public class Parcelle
    {
        /* La classe Parcelle n'est pas très utile pour le moment. 
        * Cependant, elle le sera dans le fonctionnement de l'IA du semestre 4. */

        // listeCases contient les Cases que la Parcelle regroupe.    
        protected Case[] listeCases;
        

        /*  Simple compteur du nombre de Cases que regroupe la Parcelle.
        * Le compteur servira également durant le développement de l'IA : 
        * Si, par exemple, un joueur possède plus de la moitié d'une parcelle (en l'occurrence, compteurCase), on sait qu'il n'est plus utile de jouer dedans. 
        */
        protected byte compteurCase;
        
        //NParcelle est simplement le numéro de la Parcelle.
        protected int nParcelle;

        protected string etatParcelle;
        /*
         * gagnee    
         * perdue
         * eca (en cours d'acquisition)
         * 
         */

        public Parcelle(int nParcelle)
        {
            //Création de la Parcelle, instantiation de son compteur.            
            this.compteurCase = 0;

            //On démarre une liste de cases à 6 parcelles maximum comme écrit dans l'énoncé.
            this.listeCases = new Case[6];
            this.nParcelle = nParcelle;
            this.etatParcelle = "eca";
        }
        
        public string getEtat()
        {
            return etatParcelle;
        }
        public void setEtat(string nouvelEtat)
        {
            etatParcelle = nouvelEtat;
        }
        public void estceConquis()
        {
            if (getCasesAllies() / (double)compteurCase > 0.5)
                etatParcelle = "gagnee";
            if (getCasesAdversaires() / (double)compteurCase > 0.5)
                etatParcelle = "perdue";
        }
        public void addCasetoParcelle(Case caseAAjouter)
        {
            //Fonction qui ajoute une case à la parcelle et incrémentation du compteur de cases.
            this.listeCases[compteurCase] = caseAAjouter;     
            this.compteurCase++;
            caseAAjouter.setParcelle(this.nParcelle);
        }
        public byte getTaille()
        {
            return compteurCase;
        }
        public byte getCasesAllies()
        {
            byte compteurCasesAlliees = 0;
            for(int indexListe = 0; indexListe < compteurCase; indexListe++)
            {
                if (listeCases[indexListe].getProprietaire() == 1) compteurCasesAlliees++;                    
            }
            return compteurCasesAlliees;
        }

        public byte getCasesAdversaires()
        {
            byte compteurCasesAdversaire = 0;
            for (int indexListe = 0; indexListe < compteurCase; indexListe++)
            {
                if (listeCases[indexListe].getProprietaire() == 2) compteurCasesAdversaire++;
            }
            return compteurCasesAdversaire;
        }
    }
}
