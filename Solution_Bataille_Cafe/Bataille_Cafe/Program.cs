﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net; // pour les outils réseau
using System.Net.Sockets; // pour les sockets
using System.Net.Configuration;
using System.Globalization;
using System.Xml;
using System.Runtime.InteropServices;
using System.Threading;


/*
 * 
 * 
    ---------------- HIC SUNT DRACONES ------------------- 
 * 
 * 
 * Time spent on this motherfucking project : Too much. 
 */


namespace Bataille_Cafe
{
    public class Program
    {
        private static Parcelle[] listeGlobaleDesParcelles = new Parcelle[20];
        public static List<Etendue> listeGlobaleDesEtendues = new List<Etendue>();
        
        public static Parcelle remplissageParcelle(Case caseAParcelliser, Parcelle parcelleActuelle, Case[,] plateau)
        {
            /*-----Fonction récursive remplissant une et une seule parcelle en entier.---------- 
            caseAParcelliser est la case à ajouter a parcelleActuelle et l'on se sert de plateau pour se déplacer de case en case.
           
            Après avoir ajouté caseAParcelliser dans parcelleActuelle, on s'assure qu'elle ne sera pas dans une autre parcelle via une maj de la variable booléenne
            "parcellise" de caseAParcelliser à true (setParcellisetoTrue()). 

            Suite à cela, on cherche les autres cases pouvant appartenir à la même parcelle, voir Annexe 1 pour le fonctionnement. 
            */

            parcelleActuelle.addCasetoParcelle(caseAParcelliser);
            caseAParcelliser.setParcellisetoTrue();

            //--------------N---------------------
            if (!caseAParcelliser.getFrontiere(0)) //A verifier au lancement
            {
                try
                {
                    /* 
                    * Chaque try catch sont ici dans le cas où l'on est dans la premiere ligne, c'est-à-dire que l'on a pas de Nord, (x = -1), ou première colonne càd que l'on a pas d'ouest (Y = -1), et caetera..
                    * Normalement, on n'est pas censés y entrer vu qu'il y a une frontière mais on est obligés d'ajouter un try catch, 
                    * sinon la compilation du programme s'arrête.
                    */
                    if (!plateau[caseAParcelliser.getX() - 1, caseAParcelliser.getY()].getParcellise())
                    {
                        parcelleActuelle = remplissageParcelle(plateau[caseAParcelliser.getX() - 1, caseAParcelliser.getY()], parcelleActuelle, plateau);
                    }
                }
                catch (Exception ex) { }
            }

            //--------------O---------------------
            if (!caseAParcelliser.getFrontiere(1)) //A verifier au lancement
            {
                try
                {
                    if (!plateau[caseAParcelliser.getX(), caseAParcelliser.getY() - 1].getParcellise())
                    {
                        parcelleActuelle = remplissageParcelle(plateau[caseAParcelliser.getX(), caseAParcelliser.getY() - 1], parcelleActuelle, plateau);
                    }
                }
                catch (Exception ex) { }
            }

            //--------------S---------------------
            if (!caseAParcelliser.getFrontiere(2))  //A verifier au lancement
            {
                try
                {
                    if (!plateau[caseAParcelliser.getX() + 1, caseAParcelliser.getY()].getParcellise())
                    {
                        parcelleActuelle = remplissageParcelle(plateau[caseAParcelliser.getX() + 1, caseAParcelliser.getY()], parcelleActuelle, plateau);
                    }

                }
                catch (Exception ex) { }
            }

            //--------------E---------------------
            if (!caseAParcelliser.getFrontiere(3)) //A verifier au lancement
            {
                try
                {
                    if (!plateau[caseAParcelliser.getX(), caseAParcelliser.getY() + 1].getParcellise())
                    {
                        parcelleActuelle = remplissageParcelle(plateau[caseAParcelliser.getX(), caseAParcelliser.getY() + 1], parcelleActuelle, plateau);
                    }
                }
                catch (Exception ex) { }

            }

            return parcelleActuelle;
        }

        public static void affichagePlateau(Case[,] plateau)
        {
            Console.WriteLine("\t\t\t PLATEAU DE JEU : ");
            for (int ligne = 0; ligne < 10; ligne++)
            {
                for (int colonne = 0; colonne < 10; colonne++)
                {
                    plateau[ligne, colonne].afficheMoi();
                }
                Console.WriteLine("\n");
            }
        }

        public static Case[,] initialisationPlateau(Case[,] plateauAInitialiser)
        {

            string trameAAnalyser = Reception.trameCarte;

            /* tramePipe est un tableau de 10 cases contenant les ""lignes"" du plateau de jeu. tramePipe est de la forme suivante :
            * ["67:69:69:69:69:69:69:69:69:73","74:3:9:7:5:13:3:1:9:74",...].
            
            * trameSplitee est également un tableau de 10 cases mais cette fois-ci de la forme :
            * ["67","69","69","69","69","69","69","69","69","73"].
            * Ce tableau est, par la suite, écrasé par la case suivante de tramePipe. Le processus recommence ainsi jusqu'au bout 
            * de tramePipe.
            */
            string[] tramePipe;
            string[] trameSplitee;

            //Premier découpage de la trame originale.
            
            tramePipe = trameAAnalyser.Split(new Char[] { '|' });

            //Initialisation des index du plateau
            int lignePlateau = 0;
            int colonnePlateau = 0;

            //Boucles permettant de parcourir les deux tableaux et donc créer les Cases du plateau grâce à leurs valeurs respectives de la trame.
            foreach (string ligne in tramePipe)
            {
                //Cette condition est ici pour ne pas gérer la dernière pipe toute seule.
                if (ligne != "")
                {
                    //Console.WriteLine(ligne);
                    trameSplitee = ligne.Split(':');

                    foreach (string valCase in trameSplitee)
                    {
                        //Console.WriteLine("---{0}---",valCase);                    
                        //Création de l'intégralité des cases du tableau (et de la trame)                    
                        plateauAInitialiser[lignePlateau, colonnePlateau] = new Case(int.Parse(valCase), lignePlateau, colonnePlateau);
                        colonnePlateau++;
                    }

                    //Remise de la colonne à 0
                    colonnePlateau = 0;
                    lignePlateau++;
                }
            }
            return plateauAInitialiser;
        }
        public static Case[,] parcellisationPlateau(Case[,] plateauAParcelliser)
        {

            //Création de la liste de Parcelles qui va servir à stocker les parcelles et les gérer en fonction de leur index ( de 0 à 16 ).  
            Parcelle[] listeParcelles = new Parcelle[100];
            int indexListeParcelle = 0; //Servant au parcours

            //Boucle permettant de parcourir tout le plateau(10x10).
            for (int ligne = 0; ligne < 10; ligne++)
            {
                for (int colonne = 0; colonne < 10; colonne++)
                {
                    //Vérification que la case du plateau n'est pas déjà parcellisée.
                    if (!plateauAParcelliser[ligne, colonne].getParcellise())
                    {
                        //Ajout de la nouvelle parcelle à la liste                  
                        listeParcelles[indexListeParcelle] = new Parcelle(indexListeParcelle);

                        //Remplissage récursif de la parcelle.                        
                        listeParcelles[indexListeParcelle] = remplissageParcelle(plateauAParcelliser[ligne, colonne], listeParcelles[indexListeParcelle], plateauAParcelliser);

                        //Incrémentation du compteur.
                        indexListeParcelle++;
                    }
                }
            }
            //Globalisation de la liste des Parcelles
            listeGlobaleDesParcelles = listeParcelles;
            return plateauAParcelliser;
        }

        //Cette fonction est appelée par le côté AT dès lors que l'on a reçu que le placement était valide.
        
        public static void GestionEtenduesque(Case[,] plateauDeJeu, Case caseJouee, Joueur joueurActuel)
        {
            // On fusionne l'étendu de la case avec celles qui sont adjacentes orthogonalement qui ont le même propriétaire.
            //Nord
            try
            {
                if (plateauDeJeu[caseJouee.getX(), caseJouee.getY() - 1].getProprietaire() == joueurActuel.getId())
                {
                    caseJouee.getEtendueCase().FusionEtendu(plateauDeJeu[caseJouee.getX(), caseJouee.getY() - 1].getEtendueCase(), plateauDeJeu);

                }
            }

            catch (Exception ex) { } 


            try
            {
               //Ouest
                        if (plateauDeJeu[caseJouee.getX() - 1, caseJouee.getY()].getProprietaire() == joueurActuel.getId()&&plateauDeJeu[caseJouee.getX() - 1, caseJouee.getY()].getEtendueCase() != caseJouee.getEtendueCase())
                        {
                    
                             caseJouee.getEtendueCase().FusionEtendu(plateauDeJeu[caseJouee.getX()-1, caseJouee.getY()].getEtendueCase(),plateauDeJeu);
                        }
            }
            catch(Exception ex) {
                
            }
            try
            {
             //Sud
                        if (plateauDeJeu[caseJouee.getX(), caseJouee.getY() + 1].getProprietaire() == joueurActuel.getId()&&plateauDeJeu[caseJouee.getX() , caseJouee.getY()+1].getEtendueCase() != caseJouee.getEtendueCase())
                        {
                    
                             caseJouee.getEtendueCase().FusionEtendu(plateauDeJeu[caseJouee.getX(), caseJouee.getY() + 1].getEtendueCase(),plateauDeJeu);
                        }
            }
            catch(Exception ex) { }
            try
            {
              //Est
                        if (plateauDeJeu[caseJouee.getX() + 1, caseJouee.getY()].getProprietaire() == joueurActuel.getId()&& plateauDeJeu[caseJouee.getX()+1,caseJouee.getY()].getEtendueCase()!=caseJouee.getEtendueCase())
                        {
                             
                             caseJouee.getEtendueCase().FusionEtendu(plateauDeJeu[caseJouee.getX()+1, caseJouee.getY()].getEtendueCase(),plateauDeJeu);
                   


                        }
            }
            catch(Exception ex) { }
          

        }
        public static int MeilleureTailleEtenduesque(Case[,] plateaudeJeu,Joueur joueurCible)
        {
            //Cette fonction permet de trouver la plus grande étendu associé à un joueur
            int meilleureTaille = 0;
            foreach (Etendue etendueActuelle in listeGlobaleDesEtendues)
            {
                if (etendueActuelle.getPlaceOccupeeTab() >= meilleureTaille && etendueActuelle.getProprietaire() == joueurCible.getId())
                    meilleureTaille = (int)etendueActuelle.getPlaceOccupeeTab();
            }
            return meilleureTaille;
        }

        public static int GetPlaceEtenduesAutour(Case[,] plateauDeJeu, Case CaseANoter, int proprietaire)
        {
            int pointsJointureCases = 0;
            List<Etendue> etenduesPasse = new List<Etendue>();      //Cette liste nous permet de nous assurer que l'on ne compte pas deux fois la même étendue dans le pointJointureCases
            //Nord
            try
            {
                if (plateauDeJeu[CaseANoter.getX(), CaseANoter.getY() - 1].getProprietaire() == proprietaire)
                {
                    pointsJointureCases += plateauDeJeu[CaseANoter.getX(), CaseANoter.getY() - 1].getEtendueCase().getPlaceOccupeeTab(); 
                    etenduesPasse.Add(plateauDeJeu[CaseANoter.getX(), CaseANoter.getY() - 1].getEtendueCase());
                    }

            }
            catch ( Exception ex) { }
           
            //Ouest
            try
            { 
                if (plateauDeJeu[CaseANoter.getX() - 1, CaseANoter.getY()].getProprietaire() == proprietaire && !etenduesPasse.Contains(plateauDeJeu[CaseANoter.getX()-1,CaseANoter.getY()].getEtendueCase()))
                {
                    pointsJointureCases += plateauDeJeu[CaseANoter.getX() - 1, CaseANoter.getY()].getEtendueCase().getPlaceOccupeeTab();
                    etenduesPasse.Add(plateauDeJeu[CaseANoter.getX() - 1, CaseANoter.getY()].getEtendueCase());
                } 
            }
            catch(Exception ex) { }


            //Sud
            try
            {
               if (plateauDeJeu[CaseANoter.getX(), CaseANoter.getY() + 1].getProprietaire() == proprietaire && !etenduesPasse.Contains(plateauDeJeu[CaseANoter.getX(), CaseANoter.getY() + 1].getEtendueCase()))
                        {
                            pointsJointureCases += plateauDeJeu[CaseANoter.getX(), CaseANoter.getY() + 1].getEtendueCase().getPlaceOccupeeTab();
                            etenduesPasse.Add(plateauDeJeu[CaseANoter.getX(), CaseANoter.getY() + 1].getEtendueCase());
                }
            }
            catch(Exception ex) { }
            //Est
            try
            {
                if (plateauDeJeu[CaseANoter.getX() + 1, CaseANoter.getY()].getProprietaire() == proprietaire && !etenduesPasse.Contains(plateauDeJeu[CaseANoter.getX() + 1, CaseANoter.getY() ].getEtendueCase()))
                            {
                                pointsJointureCases += plateauDeJeu[CaseANoter.getX() + 1, CaseANoter.getY()].getEtendueCase().getPlaceOccupeeTab();
                                
                            }
            }
            catch(Exception ex) { }
            

            return pointsJointureCases;
        }

        public static double CalculNotation(Case[,] plateaudeJeu, Case CaseANoter, Case CaseTourDAvant, int meilleureTailleDEtendueAllie, int meilleureTailleEtenduAdverse)
        {
            double noteCase = 0, noteEtendue = 0, noteEtendueAdv = 0;
            const double COEFFNOTEPARCELLE = 1; // Ces 3 constantes sont les poids associés à chaque paramètre établie dans la stratégie, il est donc possible de modifier la stratégie en changeant ces derniers.
            const double COEFFNOTEETENDU = 2;
            const double COEFFNOTEGENE = 0.5;
            
            double note;

            Parcelle parcelleActuelle = listeGlobaleDesParcelles[CaseANoter.getnParcelle()];
            if (CaseANoter.getJouable()&&(listeGlobaleDesParcelles[CaseTourDAvant.getnParcelle()]!=parcelleActuelle)) // On s'assure que le coup soit valide
            {
                if (parcelleActuelle.getEtat() == "eca")    //On calcul la note associé au premier paramètre, les points de parcelles. Evidemment la parcelle ne donne des points que si elle n'est ni gagné ni perdu, donc en cours d'acquisiton
                {
                    
                        //Intérêt à changer car il est complètement arbitraire.
                        int interetCase = 3 - Math.Abs(parcelleActuelle.getCasesAllies() - parcelleActuelle.getCasesAdversaires());
                        noteCase = interetCase * parcelleActuelle.getTaille() * COEFFNOTEPARCELLE;
                    
                }
                    //
                int rapportTailleEtendueAlliee = (GetPlaceEtenduesAutour(plateaudeJeu, CaseANoter, 1) + 1) - meilleureTailleDEtendueAllie;  //On a ici le nombre de point d'étendu que la graine nous offrira en jouant ici.
               
                if (rapportTailleEtendueAlliee < 0) rapportTailleEtendueAlliee = 0;
                noteEtendue = rapportTailleEtendueAlliee * COEFFNOTEETENDU;
                    

                int rapportTailleEtendueAdverse = (GetPlaceEtenduesAutour(plateaudeJeu, CaseANoter, 2) + 1) - meilleureTailleEtenduAdverse; //On a ici la gène occasionné à l'adversaire pour ces étendues. La gène des parcelles étant déjà lié au premier paramètre
                if (rapportTailleEtendueAdverse < 0) rapportTailleEtendueAdverse = 0;
                noteEtendueAdv = rapportTailleEtendueAdverse * COEFFNOTEGENE;
                note=noteCase + noteEtendue + noteEtendueAdv;

                
            }
            else
                note = 0;
            
            return note;
        }

        public static Case[,] MajPlateau(Case[,] plateaudeJeu, Case casePosee, Joueur joueurActuel)
        {
            //Fonction permettant de mettre à jour le plateau. Met à jour le propriétaire de la case,l'état des parcelles, la taille des étendues, 
            plateaudeJeu[casePosee.getX(), casePosee.getY()].setProprietaire(joueurActuel);
            listeGlobaleDesParcelles[casePosee.getnParcelle()].estceConquis();
            GestionEtenduesque(plateaudeJeu, plateaudeJeu[casePosee.getX(), casePosee.getY()], joueurActuel);
            return plateaudeJeu;
        }

        //On peut aussi appeler juste MajPlateau depuis la boucle while pour la réception de la case de l'adversaire.
        public static void DebutDeTour(Case[,] plateaudeJeu, Case caseRecue, Joueur serveur)
        {
            MajPlateau(plateaudeJeu, caseRecue, serveur);
        }
        
        public static Case HumongousIA(Case[,] plateaudeJeu, Case CaseTourDAvant,Joueur client, Joueur serveur)
        {
            Case[,] listeNotes = new Case[2, 9];
            Case CaseMieuxNotee = plateaudeJeu[0, CaseTourDAvant.getY()], CaseActuelle;

            int meilleureTailleDEtendueAllie = MeilleureTailleEtenduesque(plateaudeJeu,client);
            int meilleureTailleDEtendueAdverse = MeilleureTailleEtenduesque(plateaudeJeu,serveur);
            for (byte indexLigne = 1; indexLigne < 10; indexLigne++)
            {
                //Garde la colonne du plateau stable

                CaseActuelle = plateaudeJeu[indexLigne, CaseTourDAvant.getY()];
                //Calcul de la note de la case
               
                    CaseActuelle.setNote(CalculNotation(plateaudeJeu, CaseActuelle, CaseTourDAvant, meilleureTailleDEtendueAllie, meilleureTailleDEtendueAdverse));
               
                

                if (CaseActuelle.getNote() > CaseMieuxNotee.getNote())
                    CaseMieuxNotee = CaseActuelle;
            }

            for (byte indexColonne = 0; indexColonne < 10; indexColonne++)
            {
                //Garde la ligne du plateau stable

                CaseActuelle = plateaudeJeu[CaseTourDAvant.getX(), indexColonne];
                //Calcul de la note
              
                    CaseActuelle.setNote(CalculNotation(plateaudeJeu, CaseActuelle, CaseTourDAvant, meilleureTailleDEtendueAllie, meilleureTailleDEtendueAdverse));
           

                if (CaseActuelle.getNote() > CaseMieuxNotee.getNote())
                    CaseMieuxNotee = CaseActuelle;
            }

            
            return CaseMieuxNotee;
        }


        public static Case[,] FinDeTour(Case[,] plateaudeJeu, Case caseChoisie, Joueur client, Joueur serveur, string reponseServeur)
        {
            if (reponseServeur.Equals("VALI"))
                MajPlateau(plateaudeJeu, caseChoisie, client);
            else
                caseChoisie.setJouable(false);

            return plateaudeJeu;
        }


        public static Case PremierTour(Case[,] plateaudeJeu)
        {
            Case caseDepCouronne;
            Case caseActuelle = plateaudeJeu[5, 5];
            Case meilleureCase=plateaudeJeu[5,5];
            int transition;
            double dix = 10.0;
            for (byte couronne= 1; couronne<4; couronne++)
            {
                caseDepCouronne = caseActuelle;
                for (byte ligne = 0; ligne < couronne * 2; ligne++) 
                {
                    if (caseActuelle.getJouable() == true)
                    {
                        caseActuelle.setNote((5 - couronne) * listeGlobaleDesParcelles[caseActuelle.getnParcelle()].getTaille());

                        if (caseActuelle.getNote() > meilleureCase.getNote())
                        {
                            meilleureCase = caseActuelle;
                        }
                    }
                    for (byte colonne = 0; colonne < couronne * 2 - 1;colonne++) 
                    {
                        caseActuelle = plateaudeJeu[caseActuelle.getX(), caseActuelle.getY()-1];
                        
                        if(caseActuelle.getJouable())
                        {
                              caseActuelle.setNote(couronne * listeGlobaleDesParcelles[caseActuelle.getnParcelle()].getTaille());
                                                    if (caseActuelle.getNote() > meilleureCase.getNote())
                                                    {
                                                        meilleureCase = caseActuelle;
                                                    }
                        }
                      

                    }
                    transition = caseActuelle.getX()*10 + caseActuelle.getY();
                    transition = transition - (11 - couronne * 2);
                    if (transition < 0)
                        transition = 0;
                    caseActuelle = plateaudeJeu[(int)Math.Truncate(transition / dix), transition % 10];


                }
                caseActuelle = plateaudeJeu[caseDepCouronne.getX() + 1, caseDepCouronne.getY() + 1];
            }

            return meilleureCase;
        }
        public static void Main(string[] args)
        {

            Socket Client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp); //On définit le Socket que l'on va utiliser

            //Création des deux joueurs, client, serveur.
            Joueur client = new Joueur(1);
            Joueur serveur = new Joueur(2);
            //Création du plateau de jeu de deux dimensions, 10x10.
            Case[,] plateauDeJeu = new Case[10, 10];

            Connection.ConnexionServeur(Client);
            Reception.ReceptionTrameCarte(Client);
            
            try
            {
                plateauDeJeu = initialisationPlateau(plateauDeJeu);
            }
            catch
            {
                Console.WriteLine("Erreur,le serveur n'est pas allumé. Veuillez allumer le serveur");
                
                Console.ReadKey();
                Environment.Exit(1);

            }

            //Parcellisation du plateau.
            plateauDeJeu = parcellisationPlateau(plateauDeJeu);

            Case caseAJouer= PremierTour(plateauDeJeu);
            Envoi.Envoyage(Client, caseAJouer.getX(), caseAJouer.getY());
            Thread.Sleep(1000);
            Reception.ReceptionTrameTour(Client);
            Case caseAdverse = plateauDeJeu[Int16.Parse(Reception.placementAdverse.Substring(0, 1)), Int16.Parse(Reception.placementAdverse.Substring(1, 1))];
           
            MajPlateau(plateauDeJeu,caseAJouer, client);

            do
            { 
                Case caseTourDAvant = caseAdverse;
                //DebutDeTour(plateauDeJeu,CaseAdverse, serveur); 
                MajPlateau(plateauDeJeu, caseAdverse, serveur);
                caseAJouer = HumongousIA(plateauDeJeu,caseTourDAvant,client, serveur);
                
                int x = caseAJouer.getX(); 
                int y = caseAJouer.getY();
                Envoi.Envoyage(Client, x, y);// Il est possible de mettre les deux lignes du dessus ici.

                Thread.Sleep(1000);
                Reception.ReceptionTrameTour(Client);
                caseAdverse = plateauDeJeu[Int16.Parse(Reception.placementAdverse.Substring(0, 1)), Int16.Parse(Reception.placementAdverse.Substring(1, 1))];
                if (Reception.validiteCoup=="VALI")
                {
                    MajPlateau(plateauDeJeu,caseAJouer, client);                   
                }
                
            } while (Reception.statutJeu == "ENCO");

            Reception.AffichageScores();
            
            Console.ReadKey();
            Console.WriteLine("Voulez vous quitter le programme ?");
            
            Connection.DeconnexionServeur(Client);
            Console.ReadKey();

        }
    }
} 

            
        
