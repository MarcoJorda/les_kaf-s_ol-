﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Bataille_Cafe
{
    public static class IdentificateurCase
    {
        public static ushort identificateur = 0;
    }

    public class Case
    {
        protected int coordX;
        //Coordonnée en X.

        protected int coordY;
        //Coordonnée en Y.

        protected int type;
        /*
         * Si type = 0 -> Terrain 
         * Si type = 1 -> Forêt
         * Si type = 2 -> Mer
         */
        protected bool jouable;
        //Booléen permettant de savoir si la case est jouable ou non.

        protected byte joueurProprietaire;
        /*
         *Si proprietaire = 0 => Pas de proprietaire
         *Si proprietaire = 1 => Nous sommes le proprietaire
         *Si proprietaire = 2 => L'adversaire est proprietaire
         */

        protected int parcelleMatriarche;
        /*
         * Si parcelleMatriarche = -1, alors la case ne possède pas de parcelle 
        (Instantiation au départ).
         * Si parcelleMatriarche = 0, alors la case appartient à la parcelle 0, 
         * Si parcelleMatriarche = 1, .....et caetera. 
         */

        protected bool[] tab_frontieres = new bool[4];
        /*
         * tab_frontieres est de la forme :
         * [n,o,s,e]
         * Chaque point cardinal est un booléen
         */

        protected bool parcellise;
        // Booléen permettant de savoir si la case est parcellisé ou non (Instantiée à false par défaut). 

        protected double note;

        protected Etendue etendueDeLaCase;
        protected ushort idCase;

        public Case(int valADecoder, int coordX, int coordY)
        {
            estceJouable(valADecoder);

            //Initialisation de tab_frontieres
            for (int i = 0; i < 4; i++)
                tab_frontieres[i] = false;

            tab_frontieres = frontiere(valADecoder, tab_frontieres);
            this.joueurProprietaire = 0;
            this.coordX = coordX;
            this.coordY = coordY; 
            this.note = 0;
            this.idCase = IdentificateurCase.identificateur;
            IdentificateurCase.identificateur++;
           
            /* Cette condition sert à éviter de parcelliser une case qui n'est pas 
             jouable. 
             * Dès lors qu'une case est jouable, alors on l'instantie de sorte à ce   
             qu'elle puisse être parcellisée (Mise à false), ainsi, dans les gestions 
             de parcelles, une case non jouable sautera l'étape de vérification.

             * De plus, la future IA ne perdera pas son temps à vérifier si elle est 
             jouable car elle n'aura simplement pas de parcelle.
             */
            if (type == 0)
            {
                this.parcellise = false;
                this.etendueDeLaCase = new Etendue(idCase);
                Program.listeGlobaleDesEtendues.Add(this.etendueDeLaCase); //On add uniquement si la case est une parcelle ( != F, M donc)
            }
            else
            {
                this.parcellise = true;
               

            }
            
      
        }
        public Etendue getEtendueCase()
        {
            return this.etendueDeLaCase;
        }
        public void setEtendue(Etendue etendueAFusionner)
        {
            
            this.etendueDeLaCase = etendueAFusionner;
        }
        public ushort getId()
        {
            return idCase;
        }
        public void setProprietaire(Joueur joueurActuel)
        {
            //Si la case est contrôlée, alors elle n'est plus jouable. 
            this.etendueDeLaCase.setProprietaire(joueurActuel);
            this.joueurProprietaire = joueurActuel.getId();
            this.jouable = false;
        }
        public int getProprietaire()
        {
            return joueurProprietaire;
        }
        public void estceJouable(int valTrame)
        {
            if (valTrame > 15) // Vérification si la case est de type Terrain            
                this.jouable = false; // La case est pas jouable -> ==Forêt || ==Mer               
            else
                this.jouable = true; // La case est jouable -> ==Terrain 
        }
        public bool[] frontiere(int valTrame, bool[] tab_front)
        {
            //Recherche du type de la case

            int iteration_Type = 2;
            bool sortieBoucle = false;

            while (iteration_Type >= 0 && !sortieBoucle)
            {
                if (valTrame >= iteration_Type * 32)
                {
                    valTrame -= iteration_Type * 32;
                    this.type = iteration_Type;
                    sortieBoucle = true;
                }
                iteration_Type -= 1;
            }


            //Recherche des frontières

            for (int puissance = 3; puissance >= 0; puissance--)
            {
                if (valTrame >= Math.Pow(2, puissance))
                {
                    tab_front[puissance] = true;
                    // On remplit le tableau à l’envers, de [3] à [0].
                    valTrame -= (int)(Math.Pow(2, puissance));
                }
                // Pas besoin de else car toutes les frontières sont initialisées à true.
            }

            return tab_front;
        }
        public bool getFrontiere(int index_frontiere)
        {
            // Simple get de la frontière dont on souhaite connaître la valeur booléenne.
            // La valeur que l'on veut connaître est passée par paramètre.

            return this.tab_frontieres[index_frontiere];
        }
        public bool getJouable()
        {
            return this.jouable;
        }
        public void setJouable(bool caseJouable)
        {
            this.jouable = caseJouable;
        }
        public void afficheMoi()
        {
            //Affichage de la case de la forme = 1 F (veut dire que la case fait partie de la parcelle 1 et la case est de type F(Non jouable))

            string typeChar;

            if (type == 0)
            {
                typeChar = "T";
                Console.Write("{0} {1}\t", getnParcelle(), typeChar);
            }
            else
            {
                if (type == 1)
                    typeChar = "F";
                else
                    typeChar = "M";

                Console.Write(" {0}\t", typeChar);
            }

        }
        public int getType()
        {
            return this.type;
        }
        public bool getParcellise()
        {
            // Simple get de la valeur booléenne de "parcellisé"
            return this.parcellise;
        }
        public int getX()
        {
            // Simple get de la coordonnée X de la case.
            return this.coordX;
        }
        public int getY()
        {
            // Simple get de la coordonnée Y de la case.
            return this.coordY;
        }
        public void setParcellisetoTrue()
        {
            // Set de la variable booléenne de "parcellisé".
            this.parcellise = true;
        }
        public void setParcelle(int nParcelle)
        {
            parcelleMatriarche = nParcelle;
        }
        public int getnParcelle()
        {
            return parcelleMatriarche;
        }
        public void setNote(double note)
        {
            this.note = note;
        }
        public double getNote()
        {
            return note;
        }
    }
}

