﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bataille_Cafe
{
    public class Etendue
    {
        protected ushort[] tabIdCases;
        protected byte proprietaire;
               
        public Etendue(ushort idCase)
        {
            this.tabIdCases = new ushort[1];
            this.tabIdCases[0] = idCase;
         
        }
     
        public void FusionEtendu(Etendue etendueAManger,Case[,] plateau)
        {
            ushort[] temp = new ushort[this.getTabCases().Length + etendueAManger.getTabCases().Length];
            this.tabIdCases.CopyTo(temp, 0);
            etendueAManger.getTabCases().CopyTo(temp, this.tabIdCases.Length);
            

            this.tabIdCases = temp;
           
            foreach (ushort id in etendueAManger.getTabCases()) //On indique aux cases qui constitués l'étendueAmanger qu'elles font maintenant partie de cette étendue
            {
                plateau[(int)Math.Truncate((double)id / 10), id % 10].setEtendue(this);
                
            }
            etendueAManger = this;
            Program.listeGlobaleDesEtendues.Add(this);
        }

        public ushort[] getTabCases()
        {
            return this.tabIdCases;
        }        
        public void ajouterCase(Case caseAAjouter)
        {
            this.tabIdCases[getPlaceOccupeeTab()] = caseAAjouter.getId();
        }
        public int getPlaceOccupeeTab()
        {
            return this.tabIdCases.Length;
          
        }
        public byte getProprietaire()
        {
            return this.proprietaire;
        }
        public void setProprietaire(Joueur proprietaire)
        {
            this.proprietaire = proprietaire.getId();
        }
    

    }
}
