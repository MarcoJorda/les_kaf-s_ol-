﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net; // pour les outils réseau
using System.Net.Sockets; // pour les sockets

namespace Bataille_Cafe
{
    public static class Connection
    {

        public static void ConnexionServeur(Socket Client)
        {
            try
            {
                Client.Connect("localhost", 1213);//On se connecte au serveur en utilisant le socket client
                Console.WriteLine("Vous êtes désormais connecté! ");
            }
            catch (SocketException Erreur)
            {
                Console.WriteLine(Erreur.Message);//On retourne un message d'erreur personalisé en cas d'erreur lors de la connexion au serveur
            }
        }

        public static void DeconnexionServeur(Socket Client)
        {
            try//On essaye de déconnecter le Socket en s'assurant que les donnnées en cours de transfert soit bien traités
            {
                Console.WriteLine("Deconnection réussie");
                Client.Shutdown(SocketShutdown.Both);
            }
            finally//Enfin on détruit le Socket
            {
                Client.Close();
            }
        }

    }
}
