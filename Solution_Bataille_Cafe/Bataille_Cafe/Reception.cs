﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net; // pour les outils réseau
using System.Net.Sockets; // pour les sockets
using System.Security.Cryptography.X509Certificates;

namespace Bataille_Cafe
{
    public static class Reception
    {
        public static string carteDuJeu, validiteCoup, placementAdverse, statutJeu, scoreClient, scoreServeur, trameCarte;

        public static void ReceptionTrameTour(Socket Client)
        {
            string trameRecue=null;
            byte[] data = new byte[260];
            int data_received;
            try
            {

                //Console.WriteLine("HIC SUNT DRACONES");

                data_received = Client.Receive(data, data.Length, 0);
                trameRecue=trameRecue +Encoding.ASCII.GetString(data, 0, data_received);
               
                validiteCoup = trameRecue.Substring(0, 4);
                Console.WriteLine("Votre coup est: {0}", validiteCoup);

                placementAdverse = trameRecue.Substring(6, 2);
                Console.WriteLine("Le serveur à joué: {0}", placementAdverse);

                statutJeu = trameRecue.Substring(8, 4);
                //Console.WriteLine("Le statut du jeu est: {0} \n", statutJeu);

                if (statutJeu == "FINI")
                {
                    scoreClient = trameRecue.Substring(14, 2);
                    Console.WriteLine("Vous avez : {0} points.", scoreClient);

                    scoreServeur = trameRecue.Substring(17, 2);
                    Console.WriteLine("Le serveur a : {0} points.", scoreServeur);
                }
            }
            catch (SocketException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public static void ReceptionTrameCarte(Socket Client)
        {
            int data_received;
            byte[] data = new byte[260];

            try
            {
                data_received = Client.Receive(data, data.Length, 0);
                trameCarte =trameCarte+Encoding.ASCII.GetString(data, 0, data_received);
            }
            catch (SocketException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public static void AffichageScores()
        {
            Console.WriteLine("--- Scores finaux ---\n\tClient : {0} points.\n\tServeur : {1} points", scoreClient, scoreServeur);

            if (Int16.Parse(scoreClient) > Int16.Parse(scoreServeur))
                Console.WriteLine("Vous avez gagné avec un écart de {0} points contre le serveur.", Int16.Parse(scoreClient) -Int16.Parse(scoreServeur) );
            else
                Console.WriteLine("Vous avez perdu avec un écart de {0} points contre le serveur.", Int16.Parse(scoreServeur)-  Int16.Parse(scoreClient) );
        }
    }
}
