﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net; // pour les outils réseau
using System.Net.Sockets; // pour les sockets

namespace Bataille_Cafe
{
    public static class Envoi
    {
        public static void Envoyage(Socket Client, int coordX, int coordY)
        {
            string chaineAEnvoyer = "A:" + coordX.ToString() + coordY.ToString();            

            byte[] encodedData = new byte[260];
            Console.WriteLine("Le client a joué " + coordX + "" + coordY);
            try
            {
                encodedData = Encoding.ASCII.GetBytes(chaineAEnvoyer);
                Client.Send(encodedData, encodedData.Length, 0); 
            }
            catch (SocketException ErreurReception)
            {
                Console.WriteLine(ErreurReception.Message);
            }
        }
       
    }
}
